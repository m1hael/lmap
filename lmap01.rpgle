     /**
      * \brief Linked Map Example : Create, add and dispose
      *
      * This example simply shows how to create a map, add an item to it
      * and dispose a map.
      *
      * \author Mihael Schmidt
      * \date   05.01.2010
      */

     HDFTACTGRP(*NO) ACTGRP(*CALLER)

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
      /copy LMAP_H


      *-------------------------------------------------------------------------
      * Variables
      *-------------------------------------------------------------------------
     D map             S               *
     D value           S             50A
     D key             S             50A


      /free
       // create map
       map = lmap_create();

       // check if map is empty
       if (lmap_isEmpty(map));
         dsply 'Map is empty.';
       else;
         dsply 'Map is not empty.';
       endif;

       // add a value
       key = 'my first key';
       value = 'my first value';
       lmap_add(map : %addr(key) : %size(key) : %addr(value) : %size(value));
       dsply 'Added entry to map.';

       // check again if map is empty
       if (lmap_isEmpty(map));
         dsply 'Map is empty.';
       else;
         dsply 'Map is not empty.';
         dsply %trimr('Map has ' + %char(lmap_size(map)) + ' key(s).');
       endif;

       // free allocated memory
       lmap_dispose(map);

       *inlr = *on;
       return;
      /end-free

