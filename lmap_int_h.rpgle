      *-------------------------------------------------------------------------
      * Internal Prototypes
      *-------------------------------------------------------------------------
     D isLinkedMapImpl...
     D                 PR              N
     D   mapPtr                        *   const
      *
     D sendEscapeMessage...
     D                 PR
     D   id                          10I 0 const
      *
     D getMapEntry     PR              *
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D internal_swap   PR
     D   mapPtr                        *   const
     D   itemPtr1                      *   const
     D   itemPtr2                      *   const


      *-------------------------------------------------------------------------
      * Constants
      *-------------------------------------------------------------------------
     D HEX_NULL        C                   x'00'
     D LMAP_ID         C                   'LMAP_IMPLEMENTATION'
      *
      * Message IDs
      *
     D MSG_NO_LMAP_IMPL...
     D                 C                   1
     D MSG_NO_VALID_SORT_ALGORITHM...
     D                 C                   2
     D MSG_INVALID_VALUE_TYPE...
     D                 C                   3
     D MSG_KEY_NOT_FOUND...
     D                 C                   4


      *-------------------------------------------------------------------------
      * Variables
      *-------------------------------------------------------------------------
     D tmpl_header     DS                  qualified based(template)
     D   id                          20A
     D   heapId                      10I 0
     D   size                        10U 0
     D   firstEntry                    *
     D   lastEntry                     *
     D   iteration                   10I 0
     D   iterNextEntry...
     D                                 *
     D   iterPrevEntry...
     D                                 *
      *
     D tmpl_entry      DS                  qualified based(template)
     D   prev                          *
     D   next                          *
     D   key                           *
     D   keyLength                   10U 0
     D   value                         *
     D   valueLength                 10U 0
      *
     D hexNull         S              1A   inz(HEX_NULL)
