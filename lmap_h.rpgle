      /if not defined(LMAP)
      /define LMAP

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D lmap_create     PR              *   extproc('lmap_create')
      *
     D lmap_dispose    PR                  extproc('lmap_dispose')
     D   mapPtr                        *
      *
     D lmap_add        PR                  extproc('lmap_add')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   valuePtr                      *   const
     D   valueLength                 10U 0 const
      *
     D lmap_addInteger...
     D                 PR                  extproc('lmap_addInteger')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       10I 0 const
      *
     D lmap_addString...
     D                 PR                  extproc('lmap_addString')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                    65535A   const varying
      *
     D lmap_addLong...
     D                 PR                  extproc('lmap_addLong')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       20I 0 const
      *
     D lmap_addFloat...
     D                 PR                  extproc('lmap_addFloat')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        4F   const
      *
     D lmap_addDouble...
     D                 PR                  extproc('lmap_addDouble')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        8F   const
      *
     D lmap_addDecimal...
     D                 PR                  extproc('lmap_addDecimal')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       15P 5 const
      *
     D lmap_addDate...
     D                 PR                  extproc('lmap_addDate')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         D   const
      *
     D lmap_addTime...
     D                 PR                  extproc('lmap_addTime')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         T   const
      *
     D lmap_addTimestamp...
     D                 PR                  extproc('lmap_addTimestamp')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         Z   const
      *
     D lmap_addShort...
     D                 PR                  extproc('lmap_addShort')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        5I 0 const
      *
     D lmap_addBoolean...
     D                 PR                  extproc('lmap_addBoolean')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         N   const
      *
     D lmap_addAll     PR                  extproc('lmap_addAll')
     D   mapPtr                        *   const
     D   srcMapPtr                     *   const
      *
     D lmap_remove     PR                  extproc('lmap_remove')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D lmap_clear      PR                  extproc('lmap_clear')
     D   mapPtr                        *   const
      *
     D lmap_isEmpty    PR              N   extproc('lmap_isEmpty')
     D   mapPtr                        *   const
      *
     D lmap_get        PR              *   extproc('lmap_get')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_iterate    PR              *   extproc('lmap_iterate')
     D   mapPtr                        *   const
      *
     D lmap_abortIteration...
     D                 PR                  extproc('lmap_abortIteration')
     D   mapPtr                        *   const
      *
     D lmap_containsKey...
     D                 PR              N   extproc('lmap_containsKey')
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D lmap_containsValue...
     D                 PR              N   extproc('lmap_containsValue')
     D   mapPtr                        *   const
     D   valuePtr                      *   const
     D   valueLength                 10U 0 const
      *
     D lmap_size       PR            10I 0 extproc('lmap_size')
     D   mapPtr                        *   const
      *
     D lmap_getInteger...
     D                 PR            10I 0 extproc('lmap_getInteger')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getString...
     D                 PR         65535A   extproc('lmap_getString')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getShort...
     D                 PR             5I 0 extproc('lmap_getShort')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getLong...
     D                 PR            20I 0 extproc('lmap_getLong')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getFloat...
     D                 PR             4F   extproc('lmap_getFloat')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getDouble...
     D                 PR             8F   extproc('lmap_getDouble')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getBoolean...
     D                 PR              N   extproc('lmap_getBoolean')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getTime...
     D                 PR              T   extproc('lmap_getTime')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getTimestamp...
     D                 PR              Z   extproc('lmap_getTimestamp')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getDate...
     D                 PR              D   extproc('lmap_getDate')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const
      *
     D lmap_getDecimal...
     D                 PR            15P 5 extproc('lmap_getDecimal')
     D   mapPtr                        *   const
     D   key                           *   const
     D   keyLength                   10U 0 const

      /endif
