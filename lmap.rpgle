     /**
      * \brief Map Implementation : Linked Map
      *
      * This map implementation uses a linked list as a backend to save the data
      * and is a associative container where key/value pairs can be stored.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \link http://en.wikipedia.org/wiki/Associative_array Associative array
      * \link http://www.rpgnextgen.com?content=lmap RPG Next Gen - Linked Map
      */

      *------------------------------------------------------------------------
      *
      * Copyright (c) 2008-2010 Mihael Schmidt
      * All rights reserved.
      *
      * This file is part of the LMAP service program.
      *
      * LMAP is free software: you can redistribute it and/or modify it under
      * the terms of the GNU Lesser General Public License as published by
      * the Free Software Foundation, either version 3 of the License, or
      * any later version.
      *
      * LMAP is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU Lesser General Public License for more details.
      *
      * You should have received a copy of the GNU Lesser General Public
      * License along with LMAP.  If not, see http://www.gnu.org/licenses/.
      *
      *------------------------------------------------------------------------

     H nomain
     H copyright('Copyright (c) 2008-2010 Mihael Schmidt. All rights reserved.')

      /if defined(THREAD_SAFE)
     H THREAD(*CONCURRENT)
      /endif

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
      /include 'lmap_h.rpgle'
      /include 'lmap_int_h.rpgle'
      /include 'ceeapi_h.rpgle'
      /include 'libc_h.rpgle'


      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------

     /**
      *  \brief Create map
      *
      * Creates a map. A header is generated for the map and the pointer to
      * the map returned.
      *
      * <br><br>
      *
      * A map must be disposed via the procedure <em>dispose</em> to free all
      * allocated memory.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \return Map pointer
      */
     P lmap_create     B                   export
     D                 PI              *
      *
     D mapPtr          S               *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D heapId          S             10I 0
      /free
       cee_createHeap(heapId : *omit : *omit : *omit : *omit);

       // allocate memory for list header
       cee_getStorage(heapId : %size(tmpl_header) : mapPtr : *omit);

       header.id = LMAP_ID;
       header.heapId = heapId;
       header.size = 0;
       header.firstEntry = *null;
       header.lastEntry = *null;
       header.iteration = -1;
       header.iterNextEntry = *null;
       header.iterPrevEntry = *null;

       return mapPtr;
      /end-free
     P                 E


     /**
      * \brief Dispose map
      *
      * The memory for the whole map is released
      * (deallocated). The map pointer is set to *null;
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      */
     P lmap_dispose    B                   export
     D                 PI
     D   mapPtr                        *
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
      /free
       if (mapPtr <> *null);
         if (isLinkedMapImpl(mapPtr));
           cee_discardHeap(header.heapId : *omit);
           mapPtr = *null;
         endif;
       endif;
      /end-free
     P                 E


     /**
      * \brief Add map entry
      *
      * Adds an entry to the map. If the key already exists in the map
      * the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Pointer to the value
      * \param Value length (in byte)
      */
     P lmap_add        B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   valuePtr                      *   const
     D   valueLength                 10U 0 const
      *
     D header          DS                  likeds(tmpl_header)  based(mapPtr)
     D prevEntryPtr    S               *
     D prevEntry       DS                  likeds(tmpl_entry)
     D                                     based(prevEntryPtr)
     D newEntryPtr     S               *
     D newEntry        DS                  likeds(tmpl_entry)
     D                                     based(newEntryPtr)
     D tmpPtr          S               *
     D keyExists       S               N   inz(*off)
      /free
       if (isLinkedMapImpl(mapPtr));

         // check if key is in already in the map
         newEntryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
         if (newEntryPtr = *null);
           keyExists = *off;
         else;
           keyExists = *on;
         endif;

         if (not keyExists);    // entry not in map
           // create new entry
           cee_getStorage(header.heapId:%size(tmpl_entry):newEntryPtr:*omit);
           newEntry.keyLength = keyLength + 1;      // +1 for the null byte
           cee_getStorage(header.heapId:keyLength+1:newEntry.key:*omit);
           newEntry.valueLength = valueLength + 1;     // +1 for the null byte
           cee_getStorage(header.heapId : valueLength + 1 :
                          newEntry.value : *omit);     // +1 for the null byte
           newEntry.next = *null;
           newEntry.prev = *null;
         else;
           // entry needs to be resized
           newEntry.keyLength = keyLength + 1;
           cee_reallocateStorage(newEntry.key : keyLength + 1 : *omit); // +1 for the null byte
           newEntry.valueLength = valueLength + 1;
           cee_reallocateStorage(newEntry.value : valueLength + 1 : *omit);
         endif;

         // copy value to the list entry
         memcpy(newEntry.key : keyPtr : keyLength);
         memcpy(newEntry.value : valuePtr : valueLength);

         // set null to the last byte
         memcpy(newEntry.key + keyLength : %addr(hexNull) : 1);
         memcpy(newEntry.value + valueLength : %addr(hexNull) : 1);

         // update header
         if (not keyExists);
           header.size += 1;
           if (header.firstEntry = *null);
             header.firstEntry = newEntryPtr;
           else;
             prevEntryPtr = header.lastEntry;
             prevEntry.next = newEntryPtr;
             newEntry.prev = prevEntryPtr;
           endif;

           header.lastEntry = newEntryPtr;
         endif;
       endif;
      /end-free
     P                 E


     /**
      * \brief Add all entries
      *
      * Adds all entries from an existing map.
      *
      * <br><br>
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Pointer to destination map
      * \param Pointer to source map
      */
     P lmap_addAll     B                   export
     D                 PI
     D   mapPtr                        *   const
     D   srcMapPtr                     *   const
      *
     D header          DS                  likeds(tmpl_header) based(srcMapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry)
     D                                     based(entryPtr)
      /free
       if (isLinkedMapImpl(mapPtr) and
           isLinkedMapImpl(srcMapPtr));
         entryPtr = header.firstEntry;
         dow (entryPtr <> *null);
           lmap_add(mapPtr : entry.key : entry.keylength - 1 :
                             entry.value : entry.valueLength -1);

           entryPtr = entry.next;
         enddo;
       endif;
      /end-free
     P                 E


     /**
      * \brief Remove entry
      *
      * Removes an entry from the map. No action will be taken if the key
      * does not exist in the map.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      */
     P lmap_remove     B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D ptr             S               *
     D entry           DS                  likeds(tmpl_entry) based(ptr)
     D nextEntryPtr    S               *
     D nextEntry       DS                  likeds(tmpl_entry)
     D                                     based(nextEntryPtr)
     D prevEntryPtr    S               *
     D prevEntry       DS                  likeds(tmpl_entry)
     D                                     based(prevEntryPtr)
      /free
       if (isLinkedMapImpl(mapPtr));
         ptr = getMapEntry(mapPtr : keyPtr : keyLength);

         // check if map entry is found
         if (ptr <> *null);

           // update header
           if (header.firstEntry = ptr);
             header.firstEntry = entry.next;
           endif;
           if (header.lastEntry = ptr);
             header.lastEntry = entry.prev;
           endif;
           header.size -= 1;

           // update neighbours
           if (entry.prev <> *null);
             prevEntryPtr = entry.prev;
             prevEntry.next = entry.next;
           endif;
           if (entry.next <> *null);
             nextEntryPtr = entry.next;
             nextEntry.prev = entry.prev;
           endif;

           // release memory
           cee_freeStorage(entry.key : *omit);
           cee_freeStorage(entry.value : *omit);
           cee_freeStorage(ptr : *omit);
         endif;

       endif;
      /end-free
     P                 E


     /**
      * \brief Clear map
      *
      * Removes all entries from the map.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      *
      */
     P lmap_clear      B                   export
     D                 PI
     D   mapPtr                        *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D tmpPtr          S               *
     D ptr             S               *
     D entry           DS                  likeds(tmpl_entry) based(ptr)
      /free
       if (isLinkedMapImpl(mapPtr));

         ptr = header.lastEntry;
         dow (ptr <> *null);
           tmpPtr = entry.prev;
           cee_freeStorage(entry.key : *omit);
           cee_freeStorage(entry.value : *omit);
           cee_freeStorage(ptr : *omit);
           ptr = tmpPtr;
         enddo;

         // update header
         header.size = 0;
         header.firstEntry = *null;
         header.lastEntry = *null;
         header.iteration = -1;
         header.iterNextEntry = *null;
         header.iterPrevEntry = *null;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get next entry
      *
      * Iterates through the map and returns the key to the next entry. If the
      * iterator is at the end of the list this method will return <em>null</em>.
      * The iteration can be aborted early with the procedure <em>lmap_abortIteration</em>.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      *
      * \return Pointer to the map entry (key) or *null if there are no more entries
      */
     P lmap_iterate...
     P                 B                   export
     D                 PI              *
     D   mapPtr                        *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D retVal          S               *
     D size            S             10I 0
      /free
       if (isLinkedMapImpl(mapPtr));

         size = header.size;
         size -= 1;

         if (header.iteration = size or
             header.iteration < -1);
           header.iteration = -1;
           retVal = *null;
         else;
           if (header.iterNextEntry = *null);
             entryPtr = header.firstEntry;
           else;
             entryPtr = header.iterNextEntry;
           endif;

           header.iteration += 1;
           header.iterNextEntry = entry.next;
           retVal = entry.key;
         endif;

       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Abort iteration
      *
      * If the iteration through the list should be aborted early this
      * procedure should be called.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      */
     P lmap_abortIteration...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
      /free
       if (isLinkedMapImpl(mapPtr));
         header.iteration = -1;
         header.iterNextEntry = *null;
         header.iterPrevEntry = *null;
       endif;
      /end-free
     P                 E


     /**
      * \brief Contains key
      *
      * Checks if the key exists in the map.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      *
      * \return *on = map contains key <br>
      *         *off = map does not contain key
      */
     P lmap_containsKey...
     P                 B                   export
     D                 PI              N
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D entryPtr        S               *
     D retVal          S               N   inz(*off)
      /free
       if (isLinkedMapImpl(mapPtr));

         entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
         if (entryPtr = *null);
           retVal = *off;
         else;
           retVal = *on;
         endif;

       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Contains value
      *
      * Checks if the map contains the value.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the value
      * \param Value length (in byte)
      *
      * \return *on = map contains value <br>
      *         *off = map does not contain value
      */
     P lmap_containsValue...
     P                 B                   export
     D                 PI              N
     D   mapPtr                        *   const
     D   valuePtr                      *   const
     D   valueLength                 10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D retVal          S               N   inz(*off)
      /free
       if (isLinkedMapImpl(mapPtr));

         entryPtr = header.firstEntry;
         dow (entryPtr <> *null);

           if (valueLength = entry.valueLength - 1 and
               memcmp(valuePtr : entry.value : valueLength) = 0); // dont include the null
             retVal = *on;
             leave;
           endif;

           entryPtr = entry.next;
         enddo;

       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Is map empty
      *
      * Checks if the map is empty.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      *
      * \return *on = map is empty <br>
      *         *off = map is not empty (has at least one entry)
      */
     P lmap_isEmpty    B                   export
     D                 PI              N
     D   mapPtr                        *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D retVal          S               N
      /free
       if (isLinkedMapImpl(mapPtr));

         if (header.size = 0);
           retVal = *on;
         else;
           retVal = *off;
         endif;

       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Get value
      *
      * Returns the value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Pointer to the null-terminated value or
      *         *null if the key does not exist in the map
      */
     P lmap_get        B                   export
     D                 PI              *
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D retVal          S               *   inz(*null)
      /free
       if (isLinkedMapImpl(mapPtr));
         entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
         if (entryPtr <> *null);
           retVal = entry.value;
         endif;
       endif;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Map size
      *
      * Returns the number of entries in the map.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      *
      * \return Number of entries in the map
      */
     P lmap_size       B                   export
     D                 PI            10I 0
     D  mapPtr                         *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
      /free
       if (isLinkedMapImpl(mapPtr));
         return header.size;
       else;
         return -1;
       endif;
      /end-free
     P                 E


     /**
      * \brief Check for map implementation
      *
      * Checks if the pointer points to a map implementation.
      * The map implementation of this service program has
      * an id in the first 20 bytes of the map header.
      *
      * <br><br>
      *
      * If the pointer does not point to a map implementation an
      * escape message will be sent.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Pointer to the map
      *
      * \return *on = is map implementation <br>
      *         *off = is no map implementation (escape message)
      */
     P isLinkedMapImpl...
     P                 B
     D                 PI              N
     D   mapPtr                        *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D isMap           S               N
      /free
       monitor;
         if (header.id = LMAP_ID);
           isMap = *on;
         else;
           isMap = *off;
         endif;

         on-error *all;
           isMap = *off;
       endmon;

       if (not isMap);
         sendEscapeMessage(MSG_NO_LMAP_IMPL);
       endif;

       return isMap;
      /end-free
     P                 E


     /**
      * \brief Send Escape Message
      *
      * Sends an escape message with the specified id.
      *
      * \author Mihael Schmidt
      * \date   23.03.2008
      *
      * \param Message id
      */
     P sendEscapeMessage...
     P                 B
     D                 PI
     D   id                          10I 0 const
      *
     D sendProgramMessage...
     D                 PR                  extpgm('QMHSNDPM')
     D  szMsgID                       7A   const
     D  szMsgFile                    20A   const
     D  szMsgData                  6000A   const  options(*varsize)
     D  nMsgDataLen                  10I 0 const
     D  szMsgType                    10A   const
     D  szCallStkEntry...
     D                               10A   const
     D  nRelativeCallStkEntry...
     D                               10I 0 const
     D  szRtnMsgKey                   4A
     D  error                       265A   options(*varsize)
      *
     D msgdata         S            512A
     D msgkey          S              4A
     D apiError        S            265A
      /free
       if (id = MSG_NO_LMAP_IMPL);
         msgdata = 'The pointer does not point to a map data structure.';
       elseif (id = MSG_NO_VALID_SORT_ALGORITHM);
         msgdata = 'A non valid sort algorithm name was passed.';
       elseif (id = MSG_INVALID_VALUE_TYPE);
         msgdata = 'The requested type does not correspond to the map ' +
                   'entry type.';
       elseif (id = MSG_KEY_NOT_FOUND);
         msgdata = 'The key was not found in the map.';
       else;
         return;
       endif;

       sendProgramMessage('CPF9898' :
                          'QCPFMSG   *LIBL     ' :
                          %trimr(msgdata) :
                          %len(%trimr(msgdata)) :
                          '*ESCAPE   ' :
                          '*PGMBDY' :
                          0 :
                          msgkey :
                          apiError);
      /end-free
     P                 E


     /**
      * \brief Get entry by key
      *
      * Returns the entry associated with the passed key.
      *
      * \param Pointer to the map
      * \param Pointer to key
      * \param Length of key
      *
      * \return Pointer to the entry or null if the entry is not found
      */
     P getMapEntry     B
     D                 PI              *
     D  mapPtr                         *   const
     D  keyPtr                         *   const
     D  keyLength                    10U 0 const
      *
     D header          DS                  likeds(tmpl_header)  based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D retVal          S               *   inz(*null)
      /free
       entryPtr = header.firstEntry;

       dow (entryPtr <> *null);

         if (keyLength = entry.keyLength - 1 and
             memcmp(keyPtr : entry.key : keyLength) = 0); // dont include the null
           retVal = entryPtr;
           leave;
         endif;

         entryPtr = entry.next;
       enddo;

       return retVal;
      /end-free
     P                 E


     /**
      * \brief Swap entries
      *
      * Swaps the positions of the passed entries. The entries don't have to lay
      * next to each other.
      *
      * \param Map pointer
      * \param Pointer to entry
      * \param Pointer to entry
      */
     P internal_swap   B                   export
     D                 PI
     D   mapPtr                        *   const
     D   itemPtr1                      *   const
     D   itemPtr2                      *   const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
      *
     D entryPtr1       S               *
     D entry1          DS                  likeds(tmpl_entry) based(entryPtr1)
     D entryPtr1P      S               *
     D entry1P         DS                  likeds(tmpl_entry) based(entryPtr1P)
     D entryPtr1N      S               *
     D entry1N         DS                  likeds(tmpl_entry) based(entryPtr1N)
      *
     D entryPtr2       S               *
     D entry2          DS                  likeds(tmpl_entry) based(entryPtr2)
     D entryPtr2P      S               *
     D entry2P         DS                  likeds(tmpl_entry) based(entryPtr2P)
     D entryPtr2N      S               *
     D entry2N         DS                  likeds(tmpl_entry) based(entryPtr2N)
      *
     D tmpPtr          S               *
      /free
       // check both items point to the same entry
       if (itemPtr1 = itemPtr2);
         return;
       endif;

       entryPtr1 = itemPtr1;
       entryPtr2 = itemPtr2;

         // check if the entries are valid
         if (entryPtr1 <> *null and entryPtr2 <> *null);
           entryPtr1P = entry1.prev;
           entryPtr1N = entry1.next;
           entryPtr2P = entry2.prev;
           entryPtr2N = entry2.next;


           // check if the two nodes are neighbouring nodes
           if (entry1.next = entryPtr2);
             entry1.next = entry2.next;
             entry2.next = entryPtr1;

             entry2.prev = entry1.prev;
             entry1.prev = entryPtr2;

             if (entryPtr1P <> *null);
               entry1P.next = entryPtr2;
             endif;

             if (entryPtr2N <> *null);
               entry2N.prev = entryPtr1;
             endif;

           elseif (entry1.prev = entryPtr2); // neighbouring nodes (other way round)
             entry2.next = entry1.next;
             entry1.next = entryPtr2;

             entry1.prev = entry2.prev;
             entry2.prev = entryPtr1;


             if (entryPtr1N <> *null);
               entry1N.prev = entryPtr2;
             endif;

             if (entryPtr2P <> *null);
               entry2P.next = entryPtr1;
             endif;

           else; // no neighbours
             tmpPtr = entry1.next;
             entry1.next = entry2.next;
             entry2.next = tmpPtr;

             tmpPtr = entry1.prev;
             entry1.prev = entry2.prev;
             entry2.prev = tmpPtr;

             if (entryPtr1P <> *null);
               entry1P.next = entryPtr2;
             endif;

             if (entryPtr1N <> *null);
               entry1N.prev = entryPtr2;
             endif;

             if (entryPtr2P <> *null);
               entry2P.next = entryPtr1;
             endif;

             if (entryPtr2N <> *null);
               entry2N.prev = entryPtr1;
             endif;

           endif;


           if (entry1.prev = *null);         // check if it is the first item
             header.firstEntry = entryPtr1;
           endif;

           if (entry2.prev = *null);         // check if it is the first item
             header.firstEntry = entryPtr2;
           endif;

           if (entry1.next = *null);         // check if it is the last item
             header.lastEntry = entryPtr1;
           endif;

           if (entry2.next = *null);         // check if it is the last item
             header.lastEntry = entryPtr2;
           endif;

         endif;
      /end-free
     P                 E


     /**
      * \brief Add integer
      *
      * Adds an entry to the map with an integer value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   01.06.2008
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addInteger...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       10I 0 const
      *
     D tmpValue        S             10I 0
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : 4);
      /end-free
     P                 E


     /**
      * \brief Add string
      *
      * Adds an entry to the map with a string value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   26.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addString...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                    65535A   const varying
      *
     D tmpValue        S          65535A
      /free
       if (%len(value) = 0);
         lmap_add(mapPtr : keyPtr : keyLength : %addr(tmpValue) : 0);
       else;
         tmpValue = %subst(value : 1 : %len(value));
         lmap_add(mapPtr : keyPtr : keyLength : %addr(tmpValue) : %len(value));
       endif;
      /end-free
     P                 E


     /**
      * \brief Add long
      *
      * Adds an entry to the map with a long value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addLong...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       20I 0 const
      *
     D tmpValue        S             20I 0
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : 8);
      /end-free
     P                 E


     /**
      * \brief Add float
      *
      * Adds an entry to the map with a float value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addFloat...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        4F   const
      *
     D tmpValue        S              4F
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add double
      *
      * Adds an entry to the map with a double value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addDouble...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        8F   const
      *
     D tmpValue        S              8F
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add date
      *
      * Adds an entry to the map with a date value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addDate...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         D   const
      *
     D tmpValue        S               D
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add time
      *
      * Adds an entry to the map with a time value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addTime...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         T   const
      *
     D tmpValue        S               T
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add timestamp
      *
      * Adds an entry to the map with a timestamp value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addTimestamp...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         Z   const
      *
     D tmpValue        S               Z
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add decimal
      *
      * Adds an entry to the map with a decimal value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addDecimal...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                       15P 5   const
      *
     D tmpValue        S             15P 5
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add short
      *
      * Adds an entry to the map with a short value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addShort...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                        5I 0 const
      *
     D tmpValue        S              5I 0
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Add boolean
      *
      * Adds an entry to the map with a boolean value. If the key already
      * exists the old value will be replaced by the new one.
      *
      * \author Mihael Schmidt
      * \date   23.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in byte)
      * \param Value
      *
      */
     P lmap_addBoolean...
     P                 B                   export
     D                 PI
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
     D   value                         N   const
      *
     D tmpValue        S               N
     D valuePtr        S               *
      /free
       tmpValue = value;
       valuePtr = %addr(tmpValue);

       lmap_add(mapPtr : keyPtr : keyLength : valuePtr : %size(value));
      /end-free
     P                 E


     /**
      * \brief Get integer value
      *
      * Returns the integer value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getInteger...
     P                 B                   export
     D                 PI            10I 0
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S             10I 0 based(entry.value)
     D retVal          S             10I 0
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get string value
      *
      * Returns the string value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getString...
     P                 B                   export
     D                 PI         65535A
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S          65535A   based(entry.value)
     D retVal          S          65535A
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = %subst(temp : 1 : entry.valueLength -1);
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *blanks;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *blanks;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get short value
      *
      * Returns the short value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getShort...
     P                 B                   export
     D                 PI             5I 0
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S              5I 0 based(entry.value)
     D retVal          S              5I 0
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get long value
      *
      * Returns the long value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getLong...
     P                 B                   export
     D                 PI            20I 0
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S             20I 0 based(entry.value)
     D retVal          S             20I 0
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get float value
      *
      * Returns the float value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getFloat...
     P                 B                   export
     D                 PI             4F
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S              4F   based(entry.value)
     D retVal          S              4F
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get double value
      *
      * Returns the double value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getDouble...
     P                 B                   export
     D                 PI             8F
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S              8F   based(entry.value)
     D retVal          S              8F
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get boolean value
      *
      * Returns the boolean value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getBoolean...
     P                 B                   export
     D                 PI              N
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S               N   based(entry.value)
     D retVal          S               N
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E

     /**
      * \brief Get time value
      *
      * Returns the time value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getTime...
     P                 B                   export
     D                 PI              T
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S               T   based(entry.value)
     D retVal          S               T
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get date value
      *
      * Returns the date value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getDate...
     P                 B                   export
     D                 PI              D
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S               D   based(entry.value)
     D retVal          S               D
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get timestamp value
      *
      * Returns the timestamp value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getTimestamp...
     P                 B                   export
     D                 PI              Z
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S               Z   based(entry.value)
     D retVal          S               Z
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E


     /**
      * \brief Get decimal value
      *
      * Returns the decimal value to the passed key.
      *
      * \author Mihael Schmidt
      * \date   27.12.2009
      *
      * \param Map pointer
      * \param Pointer to the key
      * \param Key length (in Byte)
      *
      * \return Value
      *
      * \throws CPF9898 Invalid value type
      * \throws CPF9898 Key not found
      */
     P lmap_getDecimal...
     P                 B                   export
     D                 PI            15P 5
     D   mapPtr                        *   const
     D   keyPtr                        *   const
     D   keyLength                   10U 0 const
      *
     D header          DS                  likeds(tmpl_header) based(mapPtr)
     D entryPtr        S               *
     D entry           DS                  likeds(tmpl_entry) based(entryPtr)
     D temp            S             15P 5 based(entry.value)
     D retVal          S             15P 5
      /free
       isLinkedMapImpl(mapPtr);

       entryPtr = getMapEntry(mapPtr : keyPtr : keyLength);
       if (entryPtr <> *null);

         if (entry.valueLength -1 <> %size(retVal));
           sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
         endif;

         monitor;
           // test if the temp variable is filled with the right data for the type
           // by moving the data from temp to another var (testVar in this case)
           retVal = temp;
           return retVal;
           on-error *all;
             sendEscapeMessage(MSG_INVALID_VALUE_TYPE);
             return *loval;
         endmon;

       else;
         sendEscapeMessage(MSG_KEY_NOT_FOUND);
         return *loval;
       endif;
      /end-free
     P                 E
