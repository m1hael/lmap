     /**
      * \brief Linked Map Example : Iterate
      *
      * This example simply shows how to iterate through the map.
      *
      * \author Mihael Schmidt
      * \date   05.01.2010
      */

     HDFTACTGRP(*NO) ACTGRP(*CALLER)

      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D fillCustomerMap...
     D                 PR
     D   customers                     *   const
      *
     D iterateCustomers...
     D                 PR
     D   customers                     *   const
      *
      /copy LMAP_H


      *-------------------------------------------------------------------------
      * Templates
      *-------------------------------------------------------------------------
     D tmpl_customerData...
     D                 DS                  qualified based(nullPointer)
     D   name                        50A
     D   street                      50A
     D   zipcode                     10A
     D   city                        50A
     D   phone                       20A


      *-------------------------------------------------------------------------
      * Variables
      *-------------------------------------------------------------------------
     D customers       S               *

      /free
       // create map
       customers = lmap_create();

       fillCustomerMap(customers);

       iterateCustomers(customers);

       // free allocated memory
       lmap_dispose(customers);

       *inlr = *on;
       return;
      /end-free


     /**
      * \brief Fill customer map
      *
      * Fill the customer map with 3 customers and the address data of each
      * customer.
      *
      * \param Customer map
      */
     P fillCustomerMap...
     P                 B
     D                 PI
     D   customers                     *   const
      *
     D id              S             10I 0
     D data            DS                  likeds(tmpl_customerData)
      /free
       id = 1001;
       data.name = 'MS Mobile Services GmbH';
       data.street = 'Wilhelm-Rntgen-Strasse 3';
       data.zipcode = '63477';
       data.city = 'Maintal';
       data.phone = '0180 / 5 050 125';
       lmap_add(customers : %addr(id) : %size(id) : %addr(data) : %size(data));

       id = 1002;
       data.name = 'congstar GmbH';
       data.street = 'Postfach 1165';
       data.zipcode = '61466';
       data.city = 'Kronberg';
       data.phone = '01805 50 75';
       lmap_add(customers : %addr(id) : %size(id) : %addr(data) : %size(data));

       id = 1003;
       data.name = 'IKEA Deutschland GmbH & Co. KG';
       data.street = 'Am Wandersmann 2 - 4';
       data.zipcode = '65719';
       data.city = 'Hofheim-Wallau';
       data.phone = '01 80/5 35 34 35';
       lmap_add(customers : %addr(id) : %size(id) : %addr(data) : %size(data));

      /end-free
     P                 E


     /**
      * \brief Iterate through customer map
      *
      * Display the id and name of each customer.
      *
      * \param Customer map
      */
     P iterateCustomers...
     P                 B
     D                 PI
     D   customers                     *   const
      *
     D keyPtr          S               *
     D key             S             10I 0 based(keyPtr)
     D ptr             S               *
     D customerData    DS                  likeds(tmpl_customerData) based(ptr)
      /free
       keyPtr = lmap_iterate(customers);
       dow (keyPtr <> *null);
          dsply %trimr('Customer ' + %char(key));
          ptr = lmap_get(customers : keyPtr : %size(key));
          dsply customerData.name;

          keyPtr = lmap_iterate(customers);
       enddo;

      /end-free
     P                 E
