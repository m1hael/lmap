#
# Build script for Linked Map
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the service program.
# the rpg modules and the binder source file are also created in BIN_LIB.
# binder source file and rpg module can be remove with the clean step (make clean)
BIN_LIB=QGPL

# to this library the prototype source file (copy book) is copied in the install step
INCLUDE=/usr/local/include

DEF=THREAD_SAFE

TGTRLS=*CURRENT

# CFLAGS = RPG compile parameter
RCFLAGS=OPTION(*SRCSTMT) DBGVIEW(*LIST) DEFINE($(DEF)) INCDIR('$(INCLUDE)') OPTIMIZE(*BASIC) STGMDL(*INHERIT) TGTRLS($(TGTRLS))

# CCFLAGS = C compiler parameter
CCFLAGS=OPTIMIZE(30) DBGVIEW(*LIST)

# LFLAGS = binding parameter
LFLAGS=STGMDL(*INHERIT) TGTRLS($(TGTRLS))

FROM_CCSID=37

#
# User-defined part end
#-------------------------------------------------------------------------------


OBJECTS = lmap
 
 
.SUFFIXES: .rpgle .c .cpp
 
# suffix rules
.rpgle:
	system "CRTRPGMOD $(BIN_LIB)/$@ SRCSTMF('$<') $(RCFLAGS)"
        
all: clean compile bind
 
lmap:

compile: $(OBJECTS)

bind:
	-system "DLTF $(BIN_LIB)/LMAPSRV"
	-system "CRTSRCPF $(BIN_LIB)/LMAPSRV RCDLEN(112)"
	-system "CPYFRMIMPF FROMSTMF('lmap.bnd') TOFILE($(BIN_LIB)/LMAPSRV LMAP) RCDDLM(*ALL) STRDLM(*NONE) RPLNULLVAL(*FLDDFT)"
	system "CRTSRVPGM $(BIN_LIB)/LMAP MODULE($(BIN_LIB)/LMAP) $(LFLAGS) EXPORT(*SRCFILE) SRCFILE($(BIN_LIB)/LMAPSRV) TEXT('Linked Map')" 

install: lmap.bnd
	-mkdir $(INCLUDE)/lmap
	cp lmap_h.rpgle $(INCLUDE)/lmap/

clean:
	-system "DLTMOD $(BIN_LIB)/LMAP"
	-system "DLTF $(BIN_LIB)/LMAPSRV"

dist-clean: clean
	-system "DLTSRVPGM $(BIN_LIB)/LMAP"
	
.PHONY:
